/**
 */
package meeduse_tt2bdd.impl;

import meeduse_tt2bdd.InputPort;
import meeduse_tt2bdd.Meeduse_tt2bddPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Input Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InputPortImpl extends PortImpl implements InputPort {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InputPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Meeduse_tt2bddPackage.Literals.INPUT_PORT;
	}

} //InputPortImpl
