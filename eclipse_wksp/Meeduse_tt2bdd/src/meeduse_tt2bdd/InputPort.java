/**
 */
package meeduse_tt2bdd;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Input Port</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see meeduse_tt2bdd.Meeduse_tt2bddPackage#getInputPort()
 * @model
 * @generated
 */
public interface InputPort extends Port {
} // InputPort
