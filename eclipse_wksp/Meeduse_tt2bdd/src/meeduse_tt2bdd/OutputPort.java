/**
 */
package meeduse_tt2bdd;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Port</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see meeduse_tt2bdd.Meeduse_tt2bddPackage#getOutputPort()
 * @model
 * @generated
 */
public interface OutputPort extends Port {
} // OutputPort
